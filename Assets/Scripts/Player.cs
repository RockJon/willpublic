using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Player : MonoBehaviour
{
    const string PlatformTag = "Platform";

    SpriteRenderer spriteRenderer;
    CapsuleCollider2D cc2d;
    Rigidbody2D rb2d;


    [Header("Collision")]
    public float raycastLength;


    [Header("Movement")]
    public float speed;
    Vector3 currentDirection;


    [Header("Jump")]
    public float maxJumpTime;
    public float minJump, maxJump;
    float currentJump;


    [Header("Ledge Grabbing")]
    public float grabYStartPos;
    public float grabbingTime;
    public int timesToPress;
    public RectTransform pressSpaceText;
    float currentGrabbingTime;
    int currentTimesPressed;
    bool grabbing, doingGrabAnimation;
    bool leftSideGrabbing;
    //when ending grabbing the player is next to a platform which causes it to grab the platform again
    //we use this to check the player hit the ground before he can grab a platform again
    bool hasBeenGrounded;

    Transform grabPlatform;
    Animator anim;
    SpriteRenderer sp;



    bool Grounded => Colliding(transform.position, Vector3.down, cc2d.size.y / 2f);
    Transform lastPlatform;


    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        rb2d = GetComponent<Rigidbody2D>();
        cc2d = GetComponent<CapsuleCollider2D>();
        anim = GetComponent<Animator>();
        sp = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
            anim.SetTrigger("endCinematic");

        if (doingGrabAnimation)
            return;

        //Ledge grabbing
        Vector3 grabStartPos = new Vector3(transform.position.x, transform.position.y + grabYStartPos, transform.position.z);
        // Debug.Log(!Colliding(grabStartPos, Vector3.right, cc2d.size.x / 2f));
        bool leftGrabbing = (Colliding(grabStartPos, Vector3.left, cc2d.size.x / 2f));
        bool rightGrabbing = (Colliding(grabStartPos, Vector3.right, cc2d.size.x / 2f));
        if (hasBeenGrounded && !Grounded && (leftGrabbing || rightGrabbing))
        {
            leftSideGrabbing = leftGrabbing;
            grabbing = true;
            anim.SetBool("Grab",true);
            grabPlatform = lastPlatform;
            rb2d.bodyType = RigidbodyType2D.Kinematic;
            rb2d.velocity = Vector2.zero;
            hasBeenGrounded = false;
            currentDirection = Vector3.zero;
            pressSpaceText.gameObject.SetActive(true);
        }

        if (grabbing)
        {
            //Cancel grab
            KeyCode cancelGrabKey = leftSideGrabbing?  KeyCode.D : KeyCode.A;
            // Debug.Log($"{leftSideGrabbing}, {cancelGrabKey}");
            if (Input.GetKeyDown(cancelGrabKey))
            {
                EndGrab();
                return;
            }

            //Smashing check
            if (currentGrabbingTime < grabbingTime)
            {
                currentGrabbingTime += Time.deltaTime;

                if (Input.GetKeyDown(KeyCode.Space))
                {
                    pressSpaceText.localScale = Vector3.one * .5f;
                    pressSpaceText.DOKill();
                    pressSpaceText.DOScale(Vector3.one, .2f).SetEase(Ease.OutExpo);
                    currentTimesPressed++;
                }
            }
            else // end grabbing
            {
                Debug.Log($"(Pressed) {currentTimesPressed} > {timesToPress} (Needed)");
                if (currentTimesPressed > timesToPress)
                {
                    pressSpaceText.gameObject.SetActive(false);
                    doingGrabAnimation = true;
                    anim.SetBool("Grab", false);
                    float yTarget = grabPlatform.position.y + (grabPlatform.localScale.y / 2f) + (cc2d.size.y / 2f);
                    transform.DOMoveY(yTarget, 0.5f).SetEase(Ease.InOutBack).OnComplete(() =>
                    {
                        float xTarget = grabPlatform.position.x < transform.position.x? -1f : 1f;
                        transform.DOMoveX(transform.position.x + xTarget, .5f).SetEase(Ease.Linear).OnComplete(() =>
                        {
                            EndGrab();
                        });
                    });
                }
                else
                {
                    EndGrab();
                }
            }
            return;
        }


        //Movement
        bool rightSide = Colliding(transform.position, Vector3.right, cc2d.size.x / 2f, "Side");
        bool leftSide = Colliding(transform.position, Vector3.left, cc2d.size.x / 2f, "Side");
        if ((leftSide && Input.GetKey(KeyCode.A)) ||
            (rightSide && Input.GetKey(KeyCode.D)) ||
            Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
        {
            currentDirection = Vector3.zero;
            anim.SetFloat("Move", currentDirection.x);
            // print(currentDirection);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            currentDirection = Vector3.left;
            anim.SetFloat("Move", currentDirection.x);
            sp.flipX = true;
            // print(currentDirection);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            currentDirection = Vector3.right;
            anim.SetFloat("Move", currentDirection.x);
            sp.flipX = false;
            // print(currentDirection);
        }


        // if (currentDirection == Vector3.zero)
        // {
        //     if (Input.GetKeyDown(KeyCode.A))
        //         currentDirection = Vector3.left;
        //     else if (Input.GetKeyDown(KeyCode.D))
        //         currentDirection = Vector3.right;
        // }
        // else
        // {
        //     if ((currentDirection == Vector3.left && Input.GetKeyUp(KeyCode.A)) ||
        //         (currentDirection == Vector3.right && Input.GetKeyUp(KeyCode.D)))
        //     {
        //         currentDirection = Vector3.zero;
        //     }
        // }


        //Jump
        // Debug.Log($"{Grounded} && {Input.GetKey(KeyCode.Space)} && {currentJump < maxJump}");
        if (Grounded)
        {
            hasBeenGrounded = true;

            if (Input.GetKey(KeyCode.Space) && currentJump < maxJumpTime)
            {
                currentJump += Time.deltaTime;
                anim.SetBool("Squish", true);
                // Debug.Log(currentJump);
            }

            if (Input.GetKeyUp(KeyCode.Space))
            {
                anim.SetBool("Squish", false);
                var jump = minJump + ((maxJump - minJump) * (currentJump / maxJumpTime));
                // Debug.Log($"{jump} = {minJump} + (({maxJump - minJump}) * ({currentJump / maxJumpTime}))");
                rb2d.AddForce(Vector2.up * jump);
                currentJump = 0f;
            }
        }
        else
        {
            currentJump = 0f;
        }
    }

    void FixedUpdate()
    {
        // Debug.Log(Colliding(Vector3.left, cc2d.size.x / 2f));
        // Debug.Log(Colliding(Vector3.right, cc2d.size.x / 2f));

        if (grabbing)
            return;

        if (currentDirection != Vector3.zero &&
            !Colliding(transform.position, currentDirection, cc2d.size.x / 2f))
        {
            rb2d.velocity = new Vector2(currentDirection.x * speed, rb2d.velocity.y);
        }
    }

    bool Colliding(Vector3 startPosition, Vector3 direction, float raycastStartDistance, string tag = PlatformTag)
    {
        Vector2 origin = startPosition + (direction * raycastStartDistance);
        var hits = Physics2D.RaycastAll(origin, direction, raycastLength);
        Debug.DrawRay(origin, direction * raycastLength, Color.red);

        foreach(var hit in hits)
        {
            if (hit.collider.CompareTag(tag))
            {
                // Debug.Log(hit.collider.name);
                lastPlatform = hit.collider.transform;
                return true;
            }
        }
        return false;
    }

    void EndGrab()
    {
        rb2d.bodyType = RigidbodyType2D.Dynamic;
        rb2d.velocity = Vector2.zero;
        doingGrabAnimation = false;
        grabbing = false;
        anim.SetBool("Grab", false);
        currentGrabbingTime = 0f;
        currentTimesPressed = 0;
        pressSpaceText.gameObject.SetActive(false);
    }
}
