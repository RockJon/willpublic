using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Cinemachine;
using DG.Tweening;

public class FinalSequence : MonoBehaviour
{
    public CinemachineVirtualCamera virtualCamera;
    public Image foreground;

    BoxCollider2D boxCollider2D;

    float VirtualCameraSize
    {
        get { return virtualCamera.m_Lens.OrthographicSize; }
        set { virtualCamera.m_Lens.OrthographicSize = value; }
    }

    bool endedGame;

    void Awake()
    {
        boxCollider2D = GetComponent<BoxCollider2D>();
    }

    public void Show(Player player)
    {
        float duration = 7f;

        player.enabled = false;
        player.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        player.transform.DOMoveX(player.transform.position.x + 10f, duration);
        player.GetComponent<SpriteRenderer>().flipX = false;
        player.GetComponent<Animator>().SetFloat("Move", 1f);
        player.GetComponent<Animator>().SetTrigger("WalkRight");

        DOTween.To(()=> VirtualCameraSize, x=> VirtualCameraSize = x, 6f, duration)
            .SetEase(Ease.InSine).OnComplete(() =>
        {
            player.GetComponent<Animator>().SetTrigger("endCinematic");
            foreground.DOColor(Color.white, 1.5f).SetDelay(3.5f);
            DOVirtual.DelayedCall(3f, () =>
            {
                player.transform.DOMoveY(player.transform.position.y + 10f, 3f)
                    .OnComplete(() => SceneManager.LoadScene("MainMenu"));
            });
        });
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            Show(col.GetComponent<Player>());
            boxCollider2D.enabled = false;
        }
    }
}
