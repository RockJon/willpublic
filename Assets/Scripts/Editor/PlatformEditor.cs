using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Platform))]
public class PlatformEditor : Editor
{
    // bool checkedPlatforms;

    // void Awake()
    // {
    //     if (checkedPlatforms)
    //         return;

    //     checkedPlatforms = true;

    //     var platforms = GameObject.FindGameObjectsWithTag("Platform");
    //     foreach(var platform in platforms)
    //     {
    //         var scale = platform.transform.localScale;

    //         var spriteRenderer = platform.GetComponent<SpriteRenderer>();
    //         spriteRenderer.size = new Vector2(scale.x, scale.y);

    //         var boxCollider2D = platform.GetComponent<BoxCollider2D>();
    //         boxCollider2D.size = new Vector2(scale.x, scale.y);

    //         platform.transform.localScale = Vector3.one;
    //     }
    // }

    public override void OnInspectorGUI()
    {
        var platform = (target as Platform);
        var spriteRenderer = platform.GetComponent<SpriteRenderer>();
        var boxCollider2D = platform.GetComponent<BoxCollider2D>();
        boxCollider2D.size = spriteRenderer.size;
    }
}
