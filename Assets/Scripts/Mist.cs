using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Linq;

public class Mist : MonoBehaviour
{
    public Transform player;
    public float yDiff;
    public float xDistance = 110f;

    public float minDuration, maxDuration;
    public float minWaitToShow, maxWaitToShow;

    SpriteRenderer spriteRenderer;
    Image image;
    List<Ease> eases;
    bool animating;

    void Start()
    {
        eases = ((Ease[]) System.Enum.GetValues(typeof(Ease))).ToList();
        eases.Remove(Ease.InBounce);
        eases.Remove(Ease.OutBounce);
        eases.Remove(Ease.InOutBounce);

        spriteRenderer = GetComponent<SpriteRenderer>();
        image = GetComponent<Image>();
    }

    void Update()
    {
        if (animating)
            return;

        animating = true;

        float x = -1500f;
        float y = yDiff;
        if (player != null)
        {
            x = -xDistance;
            y = Random.Range(player.position.y - yDiff, player.position.y + yDiff);

        }
        transform.position = new Vector2(x, y);

        if (image == null)
        {
            spriteRenderer.color = Color.white;
            spriteRenderer.DOColor(Color.clear, 2f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
        }
        else
        {
            image.color = Color.white;
            image.DOColor(Color.clear, 2f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
        }

        float duration = Random.Range(minDuration, maxDuration);
        Ease ease = eases[Random.Range(0, eases.Count)];
        transform.DOMoveX(xDistance, duration)
            .SetEase(ease).OnComplete(() =>
        {
            if (image == null)
            {
                spriteRenderer.DOKill();
                spriteRenderer.DOColor(Color.clear, .5f).SetEase(Ease.Linear);
            }
            else
            {
                image.DOKill();
                image.DOColor(Color.clear, .5f).SetEase(Ease.Linear);
            }

            float timeToWait = Random.Range(minWaitToShow, maxWaitToShow);
            DOVirtual.DelayedCall(timeToWait, () => animating = false);
        });
    }
}
