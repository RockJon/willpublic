using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject mainScreen;
    public GameObject credits;

   public void StartPressed()
   {
       SceneManager.LoadScene("Main");
   }

   public void Credits()
   {
       credits.SetActive(true);
       mainScreen.SetActive(false);
   }

   public void CreditsBack()
   {
       credits.SetActive(false);
       mainScreen.SetActive(true);
   }

   public void Exit()
   {
       Application.Quit();
   }
}
